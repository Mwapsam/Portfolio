![](https://img.shields.io/badge/Microverse-blueviolet)

# Pedro's Portfolio

> This is my first ambiguous project of my personal portfolio.

In this portfolio there's all my projects, biography and skills shown.

## Live Server

In the following link, you can view my [portfolio's website](https://guerreiropedr0.github.io/Portfolio/).

## Built With

- HTML/CSS
- Visual Studio Code

## Getting Started

### Prerequisites

- HTML;
- CSS;
- Linters.

### Setup

To setup this repository you need to git clone (HTTPS or SSH) this repo.

## Authors

👤 **Pedro Guerreiro**

- GitHub: [@guerreiropedr0](https://github.com/guerreiropedr0)
- Twitter: [@guerreiropedr0](https://twitter.com/guerreiropedr0)
- LinkedIn: [Pedro Guerreiro](https://www.linkedin.com/in/guerreiropedr0/)

## 🤝 Contributing

Contributions, issues, and feature requests are welcome!

Feel free to check the [issues page](../../issues/).

## Show your support

Give a ⭐️ if you like this project!

## Acknowledgments

- Thank you Microverse for the opportunity
- Happy coding

## 📝 License

This project is [MIT](./MIT.md) licensed.
