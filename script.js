const ICON = document.querySelector('#icon');
const NAV_COLLAPSE = document.querySelector('#navcollapse');
const LOGO = document.querySelector('#logo');
const LISTED_ITEM = document.querySelectorAll('#navcollapse li');

ICON.addEventListener('click', () => {
  NAV_COLLAPSE.classList.toggle('navcollapse');
  ICON.classList.add('navcollapse');
  LOGO.classList.add('navcollapse');
});

LISTED_ITEM.forEach((li) => {
  li.addEventListener('click', () => {
    NAV_COLLAPSE.classList.toggle('navcollapse');
    ICON.classList.remove('navcollapse');
    LOGO.classList.remove('navcollapse');
  });
});

const PROJECT_SECTION = document.querySelector('#project-section');

const projects = [
  {
    image: './images/project1/mobile.png',
    alternative: 'Placeholder',
    title: 'To-do List',
    titleSpan: 'JavaScript project',
    href: '#',
    languages: ['HTML', 'CSS', 'JavaScript', 'Webpack'],
    webButton: 'See Project',
    modalImgMobile: './images/project1/mobile.png',
    modalBtnClose: './images/modal/close-btn.png',
    modalTitle: 'To-do List',
    modalText:
      'To-do List is a website where you can add, remove, and edit tasks. You can complete and clear them after.',
    modalLngMobile: ['HTML', 'CSS', 'JavaScript', 'Webpack'],
    modalLinks: ['See Live', 'See Source'],
  },
  {
    modalImgDesktop: './images/project1/desktop.png',
    modalBtnCloseDesktop: './images/modal/close-btn-desktop.png',
    modalTitle: 'Keeping track of hundreds of components',
    modalText:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it 1960s with the releaLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it 1960s with the releorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum han printer took a galley of type and scrambled it 1960s with the releawn printer took a galley of type and scrambled it 1960s with the releaLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it 1960s with the relea",
    modalLngDesktop: [
      'Codekit',
      'GitHub',
      'JavaScript',
      'Bootstrap',
      'Terminal',
      'Codepen',
    ],
    modalLinkLive: 'See Live',
    modalLinkSource: 'See Source',
  },
];

for (let i = 0; i < 6; i += 1) {
  const EACH_PROJECT = document.createElement('div');
  EACH_PROJECT.classList.add('flex-container');

  const img = document.createElement('img');
  img.src = projects[0].image;
  img.alt = projects[0].alternative;
  img.classList.add('placehold');

  const h3 = document.createElement('h3');
  h3.innerHTML = projects[0].title;

  const breakLine = document.createElement('br');

  const h3Span = document.createElement('span');
  h3Span.innerHTML = projects[0].titleSpan;

  const unorderedList = document.createElement('ul');

  const listedItem1 = document.createElement('li');

  listedItem1.innerHTML = [projects[0].languages[0]];

  const listedItem2 = document.createElement('li');
  listedItem2.innerHTML = [projects[0].languages[1]];

  const listedItem3 = document.createElement('li');
  listedItem3.innerHTML = [projects[0].languages[2]];

  const listedItem4 = document.createElement('li');
  listedItem4.innerHTML = [projects[0].languages[3]];

  const button = document.createElement('a');
  button.classList.add('web-button', 'center-align');
  button.href = projects[0].href;
  button.innerHTML = projects[0].webButton;
  button.id = 'button-link';

  unorderedList.appendChild(listedItem1);
  unorderedList.appendChild(listedItem2);
  unorderedList.appendChild(listedItem3);
  unorderedList.appendChild(listedItem4);
  h3.appendChild(breakLine);
  h3.appendChild(h3Span);
  EACH_PROJECT.appendChild(img);
  EACH_PROJECT.appendChild(h3);
  EACH_PROJECT.appendChild(unorderedList);
  EACH_PROJECT.appendChild(button);
  PROJECT_SECTION.appendChild(EACH_PROJECT);
}

for (let i = 1; i < projects.length; i += 1) {
  const BUTTON_LINK = document.querySelectorAll('#button-link');
  const MODAL_CONTAINER = document.querySelector('#modal-container');

  const modal = document.createElement('div');
  modal.id = 'modal';
  modal.classList.add('modal');

  const modalImgMobile = document.createElement('img');
  modalImgMobile.src = projects[0].modalImgMobile;
  modalImgMobile.alt = 'Image for mobile modal';
  modalImgMobile.classList.add('modal-img');

  const modalImgDesktop = document.createElement('img');
  modalImgDesktop.src = projects[1].modalImgDesktop;
  modalImgDesktop.alt = 'Image for desktop modal';
  modalImgDesktop.classList.add('modal-img2');

  const modalBtnClose = document.createElement('img');
  modalBtnClose.src = projects[0].modalBtnClose;
  modalBtnClose.classList.add('close-mobile');
  modalBtnClose.id = 'close';
  modalBtnClose.alt = 'Close button for modal';
  modalBtnClose.querySelector('#close');

  const modalBtnCloseDesktop = document.createElement('img');
  modalBtnCloseDesktop.src = projects[1].modalBtnCloseDesktop;
  modalBtnCloseDesktop.classList.add('close-desktop');
  modalBtnCloseDesktop.id = 'close';
  modalBtnCloseDesktop.alt = 'Close button for modal';
  modalBtnCloseDesktop.querySelector('#close');

  const modalBody = document.createElement('div');
  modalBody.classList.add('modal-body');

  const modalTitle = document.createElement('h2');
  modalTitle.innerHTML = projects[0].modalTitle;
  modalTitle.classList.add('modal-h2');

  const modalTitleDesktop = document.createElement('h2');
  modalTitleDesktop.innerHTML = projects[0].modalTitle;

  const modalList = document.createElement('ul');
  modalList.classList.add('ul-mobile');

  const modalListItem1 = document.createElement('li');
  modalListItem1.innerHTML = [projects[0].modalLngMobile[0]];

  const modalListItem2 = document.createElement('li');
  modalListItem2.innerHTML = [projects[0].modalLngMobile[1]];

  const modalListItem3 = document.createElement('li');
  modalListItem3.innerHTML = [projects[0].modalLngMobile[2]];

  const modalText = document.createElement('p');
  modalText.innerHTML = projects[0].modalText;

  const modalFooter = document.createElement('div');
  modalFooter.classList.add('modal-footer');

  const modalHeader = document.createElement('div');
  modalHeader.classList.add('modal-header');

  const seeLiveBtn = document.createElement('a');
  seeLiveBtn.innerHTML = [projects[0].modalLinks[0]];
  seeLiveBtn.href = 'https://guerreiropedr0.github.io/To-do-list/';
  seeLiveBtn.classList.add('web-button');

  const btnLiveIcon = document.createElement('img');
  btnLiveIcon.src = './images/modal/live-icon.png';
  btnLiveIcon.alt = 'Live Icon for Button';
  seeLiveBtn.appendChild(btnLiveIcon);

  const seeSourceBtn = document.createElement('a');
  seeSourceBtn.innerHTML = [projects[0].modalLinks[1]];
  seeSourceBtn.href = 'https://github.com/guerreiropedr0/To-do-list';
  seeSourceBtn.classList.add('web-button');

  const btnGitHubIcon = document.createElement('img');
  btnGitHubIcon.src = './images/modal/github-icon.png';
  btnGitHubIcon.alt = 'GitHub Icon for Button';
  seeSourceBtn.appendChild(btnGitHubIcon);

  const seeLiveBtnDesktop = document.createElement('a');
  seeLiveBtnDesktop.innerHTML = [projects[0].modalLinks[0]];
  seeLiveBtnDesktop.href = 'https://guerreiropedr0.github.io/To-do-list/';
  seeLiveBtnDesktop.classList.add('web-button');

  const btnLiveIconDesktop = document.createElement('img');
  btnLiveIconDesktop.src = './images/modal/live-icon.png';
  btnLiveIconDesktop.alt = 'Live Icon for Button';
  seeLiveBtnDesktop.appendChild(btnLiveIconDesktop);

  const seeSourceBtnDesktop = document.createElement('a');
  seeSourceBtnDesktop.innerHTML = [projects[0].modalLinks[1]];
  seeSourceBtnDesktop.href = 'https://github.com/guerreiropedr0/To-do-list';
  seeSourceBtnDesktop.classList.add('web-button');

  const btnGitHubIconDesktop = document.createElement('img');
  btnGitHubIconDesktop.src = './images/modal/github-icon.png';
  btnGitHubIconDesktop.alt = 'GitHub Icon for Button';
  seeSourceBtnDesktop.appendChild(btnGitHubIconDesktop);

  modalList.appendChild(modalListItem1);
  modalList.appendChild(modalListItem2);
  modalList.appendChild(modalListItem3);

  modalFooter.appendChild(seeLiveBtn);
  modalFooter.appendChild(seeSourceBtn);

  const breakLine = document.createElement('br');
  modalTitleDesktop.appendChild(breakLine);

  seeSourceBtnDesktop.appendChild(btnGitHubIconDesktop);
  seeLiveBtnDesktop.appendChild(btnLiveIconDesktop);
  modalHeader.appendChild(modalTitleDesktop);
  modalHeader.appendChild(seeLiveBtnDesktop);
  modalHeader.appendChild(seeSourceBtnDesktop);

  modalBody.appendChild(modalList);
  modalBody.appendChild(modalText);
  modalBody.appendChild(modalTitle);
  modalBody.appendChild(modalBtnCloseDesktop);
  modalBody.appendChild(modalBtnClose);
  modalBody.appendChild(modalImgDesktop);
  modalBody.appendChild(modalImgMobile);
  modalBody.appendChild(modalHeader);
  modal.appendChild(modalBody);
  modal.appendChild(modalFooter);

  BUTTON_LINK.forEach((btn) => {
    btn.addEventListener('click', () => {
      MODAL_CONTAINER.appendChild(modal);
      MODAL_CONTAINER.classList.remove('collapse');
    });
  });

  modalBtnClose.addEventListener('click', () => {
    MODAL_CONTAINER.classList.add('collapse');
  });

  modalBtnCloseDesktop.addEventListener('click', () => {
    MODAL_CONTAINER.classList.add('collapse');
  });

  const modalListDesktop = document.createElement('ul');
  modalListDesktop.classList.add('ul-desktop');

  const modalListItem1Desktop = document.createElement('li');
  modalListItem1Desktop.innerHTML = [projects[1].modalLngDesktop[0]];

  const modalListItem2Desktop = document.createElement('li');
  modalListItem2Desktop.innerHTML = [projects[1].modalLngDesktop[1]];

  const modalListItem3Desktop = document.createElement('li');
  modalListItem3Desktop.innerHTML = [projects[1].modalLngDesktop[2]];

  const modalListItem4Desktop = document.createElement('li');
  modalListItem4Desktop.innerHTML = [projects[1].modalLngDesktop[3]];

  const modalListItem5Desktop = document.createElement('li');
  modalListItem5Desktop.innerHTML = [projects[1].modalLngDesktop[4]];

  const modalListItem6Desktop = document.createElement('li');
  modalListItem6Desktop.innerHTML = [projects[1].modalLngDesktop[5]];

  modalListDesktop.appendChild(modalListItem1Desktop);
  modalListDesktop.appendChild(modalListItem2Desktop);
  modalListDesktop.appendChild(modalListItem3Desktop);
  modalListDesktop.appendChild(modalListItem4Desktop);
  modalListDesktop.appendChild(modalListItem5Desktop);
  modalListDesktop.appendChild(modalListItem6Desktop);

  modalBody.appendChild(modalListDesktop);
}

const form = document.querySelector('form');
const email = document.getElementById('email');
const small = document.querySelector('small');

form.addEventListener('submit', (event) => {
  if (email.validity.valueMissing) {
    small.innerHTML = 'You need to enter an e-mail address';
    event.preventDefault();
    small.classList.remove('collapse');
  } else if (email.validity.typeMismatch) {
    small.innerHTML = 'E-mail address needs to be valid (example@org.com)';
    event.preventDefault();
    small.classList.remove('collapse');
  } else if (email.validity.patternMismatch) {
    small.innerHTML = 'E-mail address needs to be in lower case';
    event.preventDefault();
    small.classList.remove('collapse');
  }
});

email.addEventListener('click', () => {
  small.classList.add('collapse');
});
